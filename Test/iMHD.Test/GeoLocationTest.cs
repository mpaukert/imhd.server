﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iMHD.Contract;
using Xunit;

namespace iMHD.Test
{
    public class GeoLocationTest
    {
        [Fact]
        public void ComputeBearing()
        {
            double lat1 = 49.04434;
            double lon1 = 16.3147373;
            double lat2 = 49.20656;
            double lon2 = 16.599596;

            var bearing = GeoUtils.GetBearing(lat1, lon1, lat2, lon2);
            Assert.Equal(49, Math.Round(bearing));
        }
    }
}
