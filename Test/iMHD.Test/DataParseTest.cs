﻿using System.IO;
using System.Linq;
using iMHD.DataProvider.Providers.Kordis;
using iMHD.DataProvider.Providers.Zlin;
using Xunit;

namespace iMHD.Test
{
    public class DataParseTest
    {
        private KordisFtpClient _kordisFtpClient;

        public DataParseTest(
            KordisFtpClient kordisFtpClient)
        {
            _kordisFtpClient = kordisFtpClient;
        }


        [Fact]
        public void ParseZlinVehiclesTest()
        {
            string vehicleContent = "217;11701;27;4;4;3;2069;11702;7401;17.688870000000001;49.242060000000002;20:54:39 17.02.2017;180";
            var vehicle = ZlinDataProvider.ConvertToVehicles(vehicleContent);
            Assert.Equal(11701,vehicle.FirstOrDefault().LastStation);
            Assert.Equal(11702, vehicle.FirstOrDefault().StartingStation);
            Assert.Equal(7401, vehicle.FirstOrDefault().FinalStation);
        }

        [Fact]
        public void ParseKordisStationsTest()
        {
            string content = File.ReadAllText(@"D:\Dropbox\UTB\Diplomka\stops.txt");
            var vehicle = KordisDataProvider.ConvertToStations(content);
        }

        [Fact]
        public async void DownloadZipFileTest()
        {
            var zipStream = await _kordisFtpClient.DownloadFile("GTFS.zip");
            var stations = _kordisFtpClient.UnzipContent(zipStream, "stops.txt");
            var kordisStations = KordisDataProvider.ConvertToStations(stations);
        }
    }
}