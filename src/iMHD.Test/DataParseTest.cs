﻿using System;
using iMHD.Contract;
using iMHD.DataProvider;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Linq;
using System.Text;
using AutoMapper;
using iMHD.Contract.Model;
using iMHD.DataProvider.Providers.Kordis;
using iMHD.DataProvider.Providers.Zlin;
using Xunit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace iMHD.Test
{
    public class DataParseTest
    {
        private KordisFtpClient _kordisFtpClient;
        private IConfigurationRoot Configuration;
        public DataParseTest()
        {
            //Mapper.Initialize(cfg => cfg.CreateMap<Station, Station>());
            Mapper.Initialize(cfg => cfg.AddProfile(typeof(AutoMapperProfileConfiguration)));
            // Arrange
            IConfigurationRoot Configuration = new ConfigurationBuilder()
                .AddJsonFile("testsettings.json", optional: false, reloadOnChange: false)
                .Build();

            IServiceCollection services = new ServiceCollection();
            services.Configure<ClientConfiguration>(ConfigureOptions);

            services.AddOptions();
            var serviceProvider = services.BuildServiceProvider();

            // Act
            var options = serviceProvider.GetService<IOptions<ClientConfiguration>>();
            _kordisFtpClient = new KordisFtpClient(options);
        }

        private void ConfigureOptions(ClientConfiguration clientConfiguration)
        {
            clientConfiguration.KordisFtpAddress = "ftp.kordis-jmk.cz";
            clientConfiguration.KordisFtpLogin = "v003021c";
            clientConfiguration.KordisFtpPassword = "ID0NlqAUeB";
        }


        [Fact]
        public void ParseZlinVehiclesTest()
        {
            string vehicleContent = "217;11701;27;4;4;3;2069;11702;7401;17.688870000000001;49.242060000000002;20:54:39 17.02.2017;180";
            var vehicle = ZlinDataProvider.ConvertToVehicles(vehicleContent);
            Assert.Equal(11701,vehicle.FirstOrDefault().LastStation);
            Assert.Equal(11702, vehicle.FirstOrDefault().StartingStation);
            Assert.Equal(7401, vehicle.FirstOrDefault().FinalStation);
        }

        [Fact]
        public async void ParseKordisStationsTest()
        {
            var zipStream = await _kordisFtpClient.DownloadFile("jr.zip");
            var bytes = _kordisFtpClient.UnzipContent(zipStream, "Zastavky.txt");
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            var enc = Encoding.GetEncoding(1252);
            var content = enc.GetString(bytes);
            var kordisStations = KordisDataProvider.ConvertToStations(content);
        }

        [Fact]
        public async void DownloadZipFileTest()
        {
            var zipStream = await _kordisFtpClient.DownloadFile("jr.zip");
            Assert.Equal(true, zipStream.CanRead);
        }
    }
}