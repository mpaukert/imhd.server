﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iMHD.Contract;
using Xunit;

namespace iMHD.Test
{
    public class GeoLocationTest
    {
        [Fact]
        public void ComputeBearing()
        {
            double lat1 = 49.04434;
            double lon1 = 16.3147373;
            double lat2 = 49.20656;
            double lon2 = 16.599596;

            var bearing = GeoUtils.GetBearing(lat1, lon1, lat2, lon2);
            Assert.Equal(49, Math.Round(bearing));
        }

        [Fact]
        public void ComputeDistanceTest()
        {
            double lat1 = 49.0997886658;
            double lon1 = 16.4426641166;
            double lat2 = 49.0996833891;
            double lon2 = 16.4428786933;
            var distance = GeoUtils.DistanceTo(lat1, lon1, lat2, lon2);
            Assert.Equal(19.5, Math.Round(distance, 1));
        }
    }
}
