﻿using System;
using iMHD.DataProvider;
using iMHD.DataProvider.Providers.Kordis;
using Microsoft.EntityFrameworkCore;
using RecurrentTasks;

namespace iMHD.Server.UpdateTasks.Kordis
{
    public class KordisStationUpdateTask : IRunnable
    {
        private KordisStationManager _manager;

        public KordisStationUpdateTask(DbContextOptions<MhdDbContext> options, KordisFtpClient client)
        {
            var context = new MhdDbContext(options);
            _manager = new KordisStationManager(context, client);
        }

        public async void Run(ITask currentTask)
        {
            try
            {
                await _manager.UpdateStations();
            }
            catch (Exception e)
            {

            }
        }
    }
}
