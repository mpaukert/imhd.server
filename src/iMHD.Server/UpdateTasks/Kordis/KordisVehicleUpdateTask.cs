﻿using System;
using iMHD.DataProvider;
using iMHD.DataProvider.Providers.Kordis;
using Microsoft.EntityFrameworkCore;
using RecurrentTasks;

namespace iMHD.Server.UpdateTasks.Kordis
{
    public class KordisVehicleUpdateTask : IRunnable
    {
        private KordisVehicleManager _manager;

        public KordisVehicleUpdateTask(DbContextOptions<MhdDbContext> options)
        {
            var context = new MhdDbContext(options);
            _manager = new KordisVehicleManager(context);
        }

        public async void Run(ITask currentTask)
        {
            try
            {
                await _manager.UpdateVehicles();
            }
            catch (Exception e)
            {

            }
        }
    }
}