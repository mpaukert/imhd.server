﻿using System;
using System.Net.Http;
using iMHD.Contract;
using Microsoft.Extensions.Options;

namespace iMHD.Server.UpdateTasks
{
    public class UpdateClient
    {
        private HttpClient _httpClient;
        private ClientConfiguration _clientConfiguration;

        public UpdateClient(IOptions<ClientConfiguration> clientConfiguration)
        {
            _clientConfiguration = clientConfiguration.Value;
        }

        public HttpClient GetHttpClient()
        {
            if (_httpClient == null)
            {
                _httpClient = new HttpClient()
                {
                    BaseAddress = new Uri(_clientConfiguration.ServerUrl),
                    Timeout = TimeSpan.FromSeconds(_clientConfiguration.HttpTimeout)
                };
            }

            return _httpClient;
        }
    }
}
