using System;
using iMHD.DataProvider;
using iMHD.DataProvider.Providers.Zlin;
using Microsoft.EntityFrameworkCore;
using RecurrentTasks;

namespace iMHD.Server.UpdateTasks.Zlin
{
    public class ZlinStationUpdateTask : IRunnable
    {
        private ZlinStationManager _manager;

        public ZlinStationUpdateTask(DbContextOptions<MhdDbContext> options, ZlinClient client)
        {
            var context = new MhdDbContext(options);
            _manager = new ZlinStationManager(context, client);
        }

        public async void Run(ITask currentTask)
        {
            try
            {
                await _manager.UpdateStations();
            }
            catch (Exception e)
            {
            }
        }
    }
}