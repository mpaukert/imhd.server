using System;
using iMHD.DataProvider;
using iMHD.DataProvider.Providers.Zlin;
using Microsoft.EntityFrameworkCore;
using RecurrentTasks;

namespace iMHD.Server.UpdateTasks.Zlin
{
    public class ZlinVehicleUpdateTask : IRunnable
    {
        private ZlinVehicleManager _manager;

        public ZlinVehicleUpdateTask(DbContextOptions<MhdDbContext> options ,ZlinClient client)
        {
            var context = new MhdDbContext(options);
            _manager = new ZlinVehicleManager(context, client);
        }

        public async void Run(ITask currentTask)
        {
            try
            {
                await _manager.UpdateVehicles();
            }
            catch (Exception e)
            {
            }
        }
    }
}