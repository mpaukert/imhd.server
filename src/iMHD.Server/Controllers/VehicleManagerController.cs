﻿using System;
using System.Threading.Tasks;
using iMHD.DataProvider.Providers.Kordis;
using iMHD.DataProvider.Providers.Zlin;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace iMHD.Server.Controllers
{
    public class VehicleManagerController : Controller
    {
        private ZlinVehicleManager _zlinVehicleManager;
        private KordisVehicleManager _kordisVehicleManager;

        public VehicleManagerController(
            ZlinVehicleManager zlinVehicleManager,
            KordisVehicleManager kordisVehicleManager
            )
        {
            _zlinVehicleManager = zlinVehicleManager;
            _kordisVehicleManager = kordisVehicleManager;
        }

        [HttpGet]
        [Route("api/v1/vehicleManager/update/zlin")]
        public async Task<IActionResult> UpdateZlinVehicles()
        {
            await _zlinVehicleManager.UpdateVehicles();
            return Ok();
        }

        [HttpGet]
        [Route("api/v1/vehicleManager/update/kordis")]
        public async Task<IActionResult> UpdateKordisVehicles()
        {
            await _kordisVehicleManager.UpdateVehicles();
            return Ok();
        }
    }
}
