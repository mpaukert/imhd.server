﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iMHD.DataProvider.Providers.Kordis;
using iMHD.DataProvider.Providers.Zlin;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace iMHD.Server.Controllers
{
    public class StationManagerController : Controller
    {
        private ZlinStationManager _zlinStationManager;
        private KordisStationManager _kordisStationManager;

        public StationManagerController(
            ZlinStationManager zlinStationManager,
            KordisStationManager kordisStationManager
        )
        {
            _zlinStationManager = zlinStationManager;
            _kordisStationManager = kordisStationManager;
        }

        [Route("api/v1/stationManager/update/zlin")]
        public async Task<IActionResult> UpdateZlinStations()
        {
            await _zlinStationManager.UpdateStations();
            return Ok();
        }

        [Route("api/v1/stationManager/update/kordis")]
        public async Task<IActionResult> UpdateKordisStations()
        {
            await _kordisStationManager.UpdateStations();
            return Ok();
        }
    }
}
