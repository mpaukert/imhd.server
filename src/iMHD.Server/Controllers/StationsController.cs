﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iMHD.Contract.Model;
using iMHD.DataProvider.Providers.Kordis;
using iMHD.DataProvider.Providers.Zlin;
using Microsoft.AspNetCore.Mvc;

namespace iMHD.Server.Controllers
{
    public class StationController : Controller
    {
        private ZlinStationManager _zlinStationManager;
        private KordisStationManager _kordisStationManager;

        public StationController(
            ZlinStationManager zlinStationManager,
            KordisStationManager kordisStationManager)
        {
            _kordisStationManager = kordisStationManager;
            _zlinStationManager = zlinStationManager;
        }

        [HttpGet]
        [Route("api/v1/stations/all/")]
        public async Task<IActionResult> GetAllStations()
        {
            IEnumerable<Station> stationList;
            try
            {
                stationList = await _zlinStationManager.GetAllStations();
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }

            return Ok(stationList);
        }

        [HttpGet]
        [Route("api/v1/stations/zlin/")]
        public async Task<IActionResult> GetZlinStations()
        {
            IEnumerable<Station> stationList;

            try
            {
                stationList = await _zlinStationManager.GetStations();
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }

            return Ok(stationList);
        }

        [HttpGet]
        [Route("api/v1/stations/zlin/{id}")]
        public async Task<IActionResult> GetZlinStation(int id)
        {
            Station station;

            try
            {
                station = await _zlinStationManager.GetStation(id);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }

            return Ok(station);
        }

        [HttpGet]
        [Route("api/v1/stations/kordis/")]
        public async Task<IActionResult> GetKordisStations()
        {
            IEnumerable<Station> stationList;

            try
            {
                stationList = await _kordisStationManager.GetStations();
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }

            return Ok(stationList);
        }

        [HttpGet]
        [Route("api/v1/stations/kordis/{id}")]
        public async Task<IActionResult> GetKordisStation(int id)
        {
            Station station;

            try
            {
                station = await _kordisStationManager.GetStation(id);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }

            return Ok(station);
        }
    }
}
