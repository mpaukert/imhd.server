﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iMHD.Contract.Model;
using iMHD.DataProvider.Providers.Kordis;
using iMHD.DataProvider.Providers.Zlin;
using Microsoft.AspNetCore.Mvc;

namespace iMHD.Server.Controllers
{
    public class VehiclesController : Controller
    {
        private KordisVehicleManager _kordisVehicleManager;
        private ZlinVehicleManager _zlinVehicleManager;

        public VehiclesController(
            KordisVehicleManager kordisvehicleManager,
            ZlinVehicleManager zlinVehicleManager
            )
        {
            _kordisVehicleManager = kordisvehicleManager;
            _zlinVehicleManager = zlinVehicleManager;
        }

        [HttpGet]
        [Route("api/v1/vehicles/all/")]
        public async Task<IActionResult> GetAllVehicles()
        {
            List<Vehicle> vehicleList = new List<Vehicle>();

            try
            {
                var zlinVehicleList = await _zlinVehicleManager.GetVehicles();
                var kordisVehicleList = await _kordisVehicleManager.GetVehicles();

                vehicleList.AddRange(zlinVehicleList);
                vehicleList.AddRange(kordisVehicleList);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }

            return Ok(vehicleList);
        }

        [HttpGet]
        [Route("api/v1/vehicles/zlin/")]
        public async Task<IActionResult> GetZlinVehicles()
        {
            IEnumerable<Vehicle> vehicleList;

            try
            {
                vehicleList = await _zlinVehicleManager.GetVehicles();
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }

            return Ok(vehicleList);
        }

        [HttpGet]
        [Route("api/v1/vehicles/kordis/")]
        public async Task<IActionResult> GetKordisVehicles()
        {
            IEnumerable<Vehicle> vehicleList;

            try
            {
                vehicleList = await _kordisVehicleManager.GetVehicles();
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }

            return Ok(vehicleList);
        }
    }
}
