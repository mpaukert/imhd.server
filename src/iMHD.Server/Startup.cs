﻿using System;
using iMHD.Contract;
using iMHD.DataProvider;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RecurrentTasks;
using System.IO;
using AutoMapper;
using iMHD.DataProvider.Providers.Kordis;
using iMHD.DataProvider.Providers.Zlin;
using iMHD.Server.UpdateTasks;
using iMHD.Server.UpdateTasks.Kordis;
using iMHD.Server.UpdateTasks.Zlin;
using Microsoft.EntityFrameworkCore;

namespace iMHD.Server
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            if (env.IsEnvironment("Development"))
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);
            services.AddMvc();
            services.AddAutoMapper();
            services.AddRouting();

            services.Configure<ClientConfiguration>(Configuration.GetSection("ClientConfiguration"));
            services.AddDbContext<MhdDbContext>(options => options.UseMySql(Configuration.GetConnectionString("MysqlConnection")));

            services.AddSingleton<ZlinClient>();
            services.AddSingleton<UpdateClient>();
            services.AddSingleton<KordisFtpClient>();

            //Managers
            services.AddScoped<ZlinVehicleManager>();
            services.AddScoped<ZlinStationManager>();
            services.AddScoped<KordisVehicleManager>();
            services.AddScoped<KordisStationManager>();

            //background tasks
            services.AddTask<ZlinVehicleUpdateTask>();
            services.AddTask<ZlinStationUpdateTask>();
            services.AddTask<KordisVehicleUpdateTask>();
            services.AddTask<KordisStationUpdateTask>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));

            if (!env.IsDevelopment())
            {
                var logFile = Path.Combine(Directory.GetCurrentDirectory(), "log.txt");
                loggerFactory.AddFile(logFile);
            }

            app.UseStaticFiles();
            app.UseDeveloperExceptionPage();
            app.UseApplicationInsightsRequestTelemetry();
            app.UseApplicationInsightsExceptionTelemetry();
            app.UseMvc();

            //Zlin tasks
            app.StartTask<ZlinVehicleUpdateTask>(TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(10));
            app.StartTask<ZlinStationUpdateTask>(TimeSpan.FromDays(30), TimeSpan.FromSeconds(20));

            //Kordis tasks
            app.StartTask<KordisVehicleUpdateTask>(TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(15));
            app.StartTask<KordisStationUpdateTask>(TimeSpan.FromDays(30), TimeSpan.FromSeconds(30));
        }
    }
}
