﻿using System.Threading.Tasks;
using iMHD.Contract.Model;

namespace iMHD.Contract.Interfaces
{
    public interface IStationManager
    {
        Task UpdateStations();

        Task<Station> GetStation(int stationId);
    }
}