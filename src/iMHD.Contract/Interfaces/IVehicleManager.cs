﻿using System.Collections.Generic;
using System.Threading.Tasks;
using iMHD.Contract.Model;

namespace iMHD.Contract.Interfaces
{
    public interface IVehicleManager
    {
        Task UpdateVehicles();

        Task<IEnumerable<Vehicle>> GetVehicles();
    }
}