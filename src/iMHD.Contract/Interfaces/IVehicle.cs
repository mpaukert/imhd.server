﻿using System;

namespace iMHD.Contract.Interfaces
{
    public interface IVehicle
    {
        int VehicleId { get; set; }

        float Lat { get; set; }

        float Lon { get; set; }

        string LineName { get; set; }

        int Delay { get; set; }

        int LastStation { get; set; }

        string ServiceNumber { get; set; }

        string Course { get; set; }

        int StartingStation { get; set; }

        int FinalStation { get; set; }

        DateTime LastUpdate { get; set; }

        double Azimuth { get; set; }

        string ProviderName { get; set; }
    }
}