﻿namespace iMHD.Contract
{
    public class ClientConfiguration
    {
        public string ZlinBaseAddress { get; set; }

        public string ServerUrl { get; set; }

        public int HttpTimeout { get; set; }

        public string KordisFtpAddress { get; set; }

        public string KordisFtpLogin { get; set; }

        public string KordisFtpPassword { get; set; }

    }
}