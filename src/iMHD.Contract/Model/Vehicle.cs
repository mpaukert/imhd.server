﻿using System;
using iMHD.Contract.Interfaces;
using Newtonsoft.Json;

namespace iMHD.Contract.Model
{
    public class Vehicle : IVehicle, IEquatable<Vehicle>
    {
        public int VehicleId { get; set; }

        public float Lat { get; set; }

        public float Lon { get; set; }

        public string LineName { get; set; }

        [JsonIgnore]
        public bool IsBarrierLess { get; set; }

        public int Delay { get; set; }

        public int LastStation { get; set; }

        [JsonIgnore]
        public string ServiceNumber { get; set; }

        [JsonIgnore]
        public string Course { get; set; }

        public int StartingStation { get; set; }

        public int FinalStation { get; set; }

        public DateTime LastUpdate { get; set; }

        public double Azimuth { get; set; }

        public string ProviderName { get; set; }

        public int RouteId { get; set; }

        public bool Equals(Vehicle other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return VehicleId == other.VehicleId && string.Equals(ProviderName, other.ProviderName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Vehicle)) return false;
            return Equals((Vehicle) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (VehicleId * 397) ^ ProviderName.GetHashCode();
            }
        }

        public bool IsValid()
        {
            if (VehicleId <= 0)
            {
                return false;
            }

            if (string.IsNullOrEmpty(ProviderName))
            {
                return false;
            }

            if (FinalStation <= 0)
            {
                return false;
            }

            if (LastStation <= 0)
            {
                return false;
            }

            return true;
        }
    }
}
