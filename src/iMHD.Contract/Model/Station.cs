﻿using System;
using Newtonsoft.Json;

namespace iMHD.Contract.Model
{
    public class Station : IEquatable<Station>
    {
        public int StationId { get; set; }

        public string Name { get; set; }

        [JsonIgnore]
        public double Lat { get; set; }

        [JsonIgnore]
        public double Lon { get; set; }

        [JsonIgnore]
        public int RequestStop { get; set; }

        [JsonIgnore]
        public int PartiallyRequestStop { get; set; }

        [JsonIgnore]
        public int FirstDoorEntryOnly { get; set; }

        [JsonIgnore]
        public int HandicappedStop { get; set; }

        [JsonIgnore]
        public string Zone { get; set; }

        public string ProviderName { get; set; }

        public bool Equals(Station other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return StationId == other.StationId && string.Equals(ProviderName, other.ProviderName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Station)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (StationId * 397) ^ (ProviderName != null ? ProviderName.GetHashCode() : 0);
            }
        }

        public bool IsValid()
        {
            return StationId > 0 && !string.IsNullOrEmpty(ProviderName);
        }
    }
}