﻿using iMHD.Contract.Model;
using Microsoft.EntityFrameworkCore;

namespace iMHD.DataProvider
{
    public class MhdDbContext : DbContext
    {
        public MhdDbContext(DbContextOptions<MhdDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Station> Stations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Vehicle>().HasKey(k => new { k.ProviderName, k.VehicleId });
            modelBuilder.Entity<Station>().HasKey(k => new { k.ProviderName, k.StationId });

            base.OnModelCreating(modelBuilder);
        }
    }
}