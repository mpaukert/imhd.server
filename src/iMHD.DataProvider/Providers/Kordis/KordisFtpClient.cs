﻿using System.IO;
using System.IO.Compression;
using System.Threading;
using CoreFtp;
using iMHD.Contract;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using System.Text;

namespace iMHD.DataProvider.Providers.Kordis
{
    public class KordisFtpClient
    {
        private FtpClient _ftpClient;

        public KordisFtpClient(IOptions<ClientConfiguration> clientConfiguration)
        {
            _ftpClient = new FtpClient(new FtpClientConfiguration()
            {
                Host = clientConfiguration.Value.KordisFtpAddress,
                Username = clientConfiguration.Value.KordisFtpLogin,
                Password = clientConfiguration.Value.KordisFtpPassword,
            });
        }

        public async Task<Stream> DownloadFile(string fileName)
        {
            await _ftpClient.LoginAsync();
            await _ftpClient.ChangeWorkingDirectoryAsync("v003021c");

            var ftpStream = await _ftpClient.OpenFileReadStreamAsync(fileName);
            return ftpStream;
        }

        public byte[] UnzipContent(Stream fileStream, string fileName)
        {
            using (var fs = fileStream)
            {
                using (var zip = new ZipArchive(fs, ZipArchiveMode.Read, false))
                {
                    var zipEntry = zip.GetEntry(fileName);
                    return KordisDataProvider.ConvertToBytes(zipEntry.Open());
                }
            }
        }
    }
}