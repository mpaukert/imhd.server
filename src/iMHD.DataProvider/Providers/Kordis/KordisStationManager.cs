﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using iMHD.Contract.Interfaces;
using iMHD.Contract.Model;

namespace iMHD.DataProvider.Providers.Kordis
{
    public class KordisStationManager : StationManager, IStationManager
    {
        private KordisFtpClient _kordisFtpClient;

        public KordisStationManager(
            MhdDbContext context,
            KordisFtpClient kordisFtpClient
            ) : base("Kordis", context)
        {
            _kordisFtpClient = kordisFtpClient;
        }

        public async Task UpdateStations()
        {
            var stations = await GetStationsFromServer();
            try
            {
                StoreStations(stations);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        public async Task<Station> GetStation(int stationId)
        {
            var station = await base.GetStationById(stationId);
            return station;
        }

        private async Task<IEnumerable<Station>> GetStationsFromServer()
        {
            using (var fs = await _kordisFtpClient.DownloadFile("jr.zip"))
            {
                var bytes = _kordisFtpClient.UnzipContent(fs, "Zastavky.txt");
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                var enc = Encoding.GetEncoding(1252);
                var content = enc.GetString(bytes);
                var stations = KordisDataProvider.ConvertToStations(content);

                return stations;
            }
        }
    }
}
