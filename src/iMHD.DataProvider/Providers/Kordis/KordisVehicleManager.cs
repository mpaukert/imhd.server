﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using iMHD.Contract;
using iMHD.Contract.Interfaces;
using iMHD.Contract.Model;
using KordisReference;
using Microsoft.EntityFrameworkCore;

namespace iMHD.DataProvider.Providers.Kordis
{
    public class KordisVehicleManager : VehicleManager, IVehicleManager
    {
        public KordisVehicleManager(
            MhdDbContext dbContext
        ) : base("Kordis", dbContext)
        {
        }

        public async Task UpdateVehicles()
        {
            var zlinVehicles = await GetVehiclesFromServer();
            StoreVehicles(zlinVehicles);
            DeleteOutdatedVehicles(zlinVehicles);
        }

        public async Task<IEnumerable<Vehicle>> GetVehicles()
        {
            return await GetVehiclesFromDb();
        }

        private void StoreVehicles(IEnumerable<Vehicle> responseVehicles)
        {
            DbContext.Database.OpenConnection();
            foreach (var vehicle in responseVehicles)
            {
                //for each requested vehicle check if it is already in DB
                var foundVehicle = DbContext.Vehicles.FirstOrDefault(
                    x => x.VehicleId == vehicle.VehicleId && x.ProviderName == vehicle.ProviderName);

                if (foundVehicle != null)
                {
                    vehicle.Azimuth = foundVehicle.Azimuth;
                    var distance = GeoUtils.DistanceTo(vehicle.Lat, vehicle.Lon, foundVehicle.Lat, foundVehicle.Lon);

                    if (distance > 10)
                    {
                        vehicle.Azimuth = Math.Round(
                            GeoUtils.GetBearing(foundVehicle.Lat, foundVehicle.Lon, vehicle.Lat, vehicle.Lon));
                        vehicle.LastUpdate = DateTime.Now;

                        Mapper.Map(vehicle, foundVehicle);
                    }
                }
                else
                {
                    DbContext.Vehicles.Add(vehicle);
                }
            }

            DbContext.SaveChanges();
            DbContext.Database.CloseConnection();
        }

        private async Task<IEnumerable<Vehicle>> GetVehiclesFromServer()
        {
            var trafficStateClient = new TrafficStateClient();
            var traffic = await trafficStateClient.GetTrafficStateAsync();
            return KordisDataProvider.ConvertToVehicles(traffic.VehicleL);
        }
    }
}