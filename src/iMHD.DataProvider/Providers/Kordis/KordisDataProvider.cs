using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using iMHD.Contract.Model;
using KordisReference;
using Microsoft.Extensions.Logging;

namespace iMHD.DataProvider.Providers.Kordis
{
    public class KordisDataProvider
    {

        public static IEnumerable<Vehicle> ConvertToVehicles(TrafficStateResp.Entry[] vehicEntries)
        {
            var vehicleList = new List<Vehicle>();

            foreach (var kordisVehicle in vehicEntries)
            {
                var vehicle = new Vehicle()
                {
                    ProviderName = "Kordis"
                };

                Mapper.Map(kordisVehicle, vehicle);

                if (vehicle.IsValid() && !vehicleList.Contains(vehicle))
                {
                    vehicleList.Add(vehicle);
                }
            }

            return vehicleList;
        }

        public static IEnumerable<Station> ConvertToStations(string fileContent)
        {
            List<Station> stations = new List<Station>();
            fileContent = fileContent.Replace(
                "stop_id,stop_name,stop_lat,stop_lon,zone_id,location_type,parent_station,wheelchair_boarding",
                string.Empty);

            fileContent = Regex.Replace(fileContent,"  (.*?) (.*?) (.*?) (.*?) (.*?) '(.*?)'",string.Empty);

            var lines = fileContent.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var line in lines)
                {
                    MatchCollection parsedLine = Regex.Matches(line, "(.*?) (.*?) '(.*?)' '(.*?)' '(.*?)' '(.*?)' '(.*?)'");

                    if (parsedLine.Count == 0)
                    {
                        continue;
                    }

                    var station = new Station()
                    {
                        ProviderName = "Kordis"
                    };

                    Mapper.Map(parsedLine, station);
                    var foundStation =
                        stations.FirstOrDefault(x => x.StationId == station.StationId && x.ProviderName == "Kordis");
                    if (station.IsValid() && foundStation == null)
                    {
                        stations.Add(station);
                    }
                }

            return stations;
        }

        public static byte[] ConvertToBytes(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            MemoryStream ms = new MemoryStream();
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                ms.Write(buffer, 0, read);
            }
            return ms.ToArray();

        }
    }
}