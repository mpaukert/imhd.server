﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using iMHD.Contract.Interfaces;
using iMHD.Contract.Model;
using Microsoft.EntityFrameworkCore;

namespace iMHD.DataProvider.Providers.Zlin
{
    public class ZlinVehicleManager : VehicleManager, IVehicleManager
    {
        private ZlinClient _zlinClient;

        public ZlinVehicleManager(
            MhdDbContext dbContext,
            ZlinClient zlinClient
            ) : base("Zlin", dbContext)
        {
            _zlinClient = zlinClient;
        }

        public async Task UpdateVehicles()
        {
            var zlinVehicles = await GetVehiclesFromServer();
            StoreVehicles(zlinVehicles);
            DeleteOutdatedVehicles(zlinVehicles);
        }

        public async Task<IEnumerable<Vehicle>> GetVehicles()
        {
            var vehicles = await GetVehiclesFromDb();
            return vehicles;
        }

        private void StoreVehicles(IEnumerable<Vehicle> responseVehicles)
        {
            DbContext.Database.OpenConnection();
            foreach (var vehicle in responseVehicles)
            {
                //for each requested vehicle check if it is already in DB
                var foundVehicle = DbContext.Vehicles.FirstOrDefault(
                    x => x.VehicleId == vehicle.VehicleId && x.ProviderName == vehicle.ProviderName);

                if (foundVehicle != null)
                {
                    if (Math.Abs(foundVehicle.Lat - vehicle.Lat) > 0 || Math.Abs(foundVehicle.Lon - vehicle.Lon) > 0)
                    {
                        vehicle.LastUpdate = DateTime.Now;
                        Mapper.Map<Vehicle, Vehicle>(vehicle, foundVehicle);
                    }
                }
                else
                {
                    DbContext.Vehicles.Add(vehicle);
                }
            }

            DbContext.SaveChanges();
            DbContext.Database.CloseConnection();
        }

        private async Task<IEnumerable<Vehicle>> GetVehiclesFromServer()
        {
            var response = await _zlinClient.FetchUrl("online/odchylky.txt");

            var convertedVehicles = ZlinDataProvider.ConvertToVehicles(response);
            return convertedVehicles;
        }
    }
}