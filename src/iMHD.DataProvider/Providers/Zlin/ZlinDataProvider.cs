﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using iMHD.Contract.Model;

namespace iMHD.DataProvider.Providers.Zlin
{
    public class ZlinDataProvider
    {
        public static IEnumerable<Vehicle> ConvertToVehicles(string csvFile)
        {
            var lines = csvFile.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            var content = lines.Where(line => !string.IsNullOrEmpty(line));
            var splittedContent = content.Select(line => line.Split(';'));
            var vehicleList = new List<Vehicle>();

            foreach (var zlinVehicle in splittedContent)
            {
                var vehicle = new Vehicle()
                {
                    ProviderName = "Zlin"
                };

                Mapper.Map(zlinVehicle, vehicle);

                if (vehicle.IsValid() && !vehicleList.Contains(vehicle))
                {
                    vehicleList.Add(vehicle);
                }
            }

            return vehicleList;
        }

        public static IEnumerable<Station> ConvertToStations(string csvFile)
        {
            var lines = csvFile.Split(new[] { @"<br/>" }, StringSplitOptions.None);
            var content = lines.Where(line => !string.IsNullOrEmpty(line));
            var splittedContent = content.Select(line => line.Split(';'));
            List<Station> stations = new List<Station>();
            foreach (var zlinStation in splittedContent)
            {
                var station = new Station()
                {
                    ProviderName = "Zlin"
                };

                Mapper.Map(zlinStation, station);

                if (station.IsValid() && !stations.Contains(station))
                {
                    stations.Add(station);
                }
            }

            return stations;
        }
    }
}
