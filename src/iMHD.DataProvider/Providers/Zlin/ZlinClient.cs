﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using iMHD.Contract;
using Microsoft.Extensions.Options;

namespace iMHD.DataProvider.Providers.Zlin
{
    public class ZlinClient
    {
        private static HttpClient _httpClient;
        private ClientConfiguration _clientConfiguration;

        public ZlinClient(IOptions<ClientConfiguration> clientConfiguration)
        {
            _clientConfiguration = clientConfiguration.Value;
        }

        private HttpClient GetHttpClient()
        {
            if (_httpClient == null)
            {
                _httpClient = new HttpClient()
                {
                    BaseAddress = new Uri(_clientConfiguration.ZlinBaseAddress),
                    Timeout = TimeSpan.FromSeconds(_clientConfiguration.HttpTimeout)
                };
            }

            return _httpClient;
        }

        public async Task<string> FetchUrl(string relativeUrl)
        {
            var bytes = await GetHttpClient().GetByteArrayAsync(new Uri(relativeUrl, UriKind.Relative));
            var response = Encoding.UTF8.GetString(bytes);
            return response;
        }
    }
}