﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using iMHD.Contract.Model;

namespace iMHD.DataProvider.Providers.Zlin
{
    public class ZlinStationManager : StationManager
    {
        private ZlinClient _zlinClient;
        public ZlinStationManager(
            MhdDbContext context,
            ZlinClient zlinClient
            ) : base("Zlin", context)
        {
            _zlinClient = zlinClient;
        }

        public async Task UpdateStations()
        {
            var stations = await GetStationsFromServer();
            StoreStations(stations);
        }

        public async Task<Station> GetStation(int stationId)
        {
            var station = await base.GetStationById(stationId);
            return station;
        }

        private async Task<IEnumerable<Station>> GetStationsFromServer()
        {
            var response = await _zlinClient.FetchUrl("online/zastavky.php");
            var convertedStations = ZlinDataProvider.ConvertToStations(response);
            return convertedStations;
        }
    }
}
