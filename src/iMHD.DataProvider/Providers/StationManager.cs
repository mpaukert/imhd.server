﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using iMHD.Contract.Model;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;

namespace iMHD.DataProvider.Providers
{
    public abstract class StationManager
    {
        protected readonly MhdDbContext DbContext;
        private readonly string _providerName;

        protected StationManager(
            string providerName,
            MhdDbContext context
            )
        {
            _providerName = providerName;
            DbContext = context;
        }

        public async Task<IEnumerable<Station>> GetAllStations()
        {
            var stations = await DbContext.Stations.AsNoTracking().ToListAsync();

            return stations;
        }

        public async Task<IEnumerable<Station>> GetStations()
        {
            var stations = DbContext.Stations.AsNoTracking().Where(x=>x.ProviderName == _providerName);
            return await stations.ToListAsync();
        }

        public async Task<Station> GetStationById(int stationId)
        {
            var stations = DbContext.Stations.AsNoTracking().FirstOrDefaultAsync(
                x => x.ProviderName == _providerName && x.StationId == stationId);
            return await stations;
        }

        protected void StoreStations(IEnumerable<Station> stations)
        {
            int cycle = 0;
            foreach (var station in stations)
            {
                //for each requested station check if it is already in DB
                var foundStation = DbContext.Stations.FirstOrDefault(
                    x => x.StationId == station.StationId && x.ProviderName == _providerName);

                if (foundStation != null)
                {
                    Mapper.Map(station, foundStation);
                }
                else
                {
                    DbContext.Stations.Add(station);
                }

                if (stations.Count() > 1000 && cycle % 1000 == 0)
                {
                    DbContext.SaveChanges();
                }

                cycle++;
            }


            DbContext.SaveChanges();
        }

        protected void DeleteOutdatedStations(IEnumerable<Station> responseStations)
        {
            DbContext.Database.OpenConnection();
            var dbStations = DbContext.Stations.Where(x => x.ProviderName == _providerName);
            //Check if Vehicles from request contains vehicle in db otherwise delete them
            foreach (var station in dbStations)
            {
                if (!responseStations.Contains(station))
                {
                    DbContext.Stations.Remove(station);
                }
            }

            DbContext.SaveChanges();
            DbContext.Database.CloseConnection();

        }
    }
}
