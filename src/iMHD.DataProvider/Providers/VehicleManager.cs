﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iMHD.Contract.Model;
using Microsoft.EntityFrameworkCore;

namespace iMHD.DataProvider.Providers
{
    public abstract class VehicleManager
    {
        protected MhdDbContext DbContext;
        private readonly string _providerName;

        protected VehicleManager(
            string providerName,
            MhdDbContext context
            )
        {
            DbContext = context;
            _providerName = providerName;
        }

        protected async Task<IEnumerable<Vehicle>> GetVehiclesFromDb()
        {
            var vehicles = await DbContext.Vehicles.AsNoTracking()
                .Where(x => x.ProviderName == _providerName)
                .OrderBy(t=>t.LastUpdate)
                .ToListAsync();

            var filteredVehicles = vehicles.GroupBy(x => x.VehicleId).Select(g => g.First());

            return filteredVehicles;
        }

        protected async Task<IEnumerable<Vehicle>> GetVehiclesByLineName(string lineName)
        {
            var vehicles = await DbContext.Vehicles.AsNoTracking()
                .Where(x => x.LineName == lineName && x.ProviderName == _providerName).ToListAsync();

            var filteredVehicles = vehicles.OrderBy(t => t.LastUpdate)
                .GroupBy(g => g.VehicleId)
                .SelectMany(v => v);

            return filteredVehicles;
        }

        protected void DeleteOutdatedVehicles(IEnumerable<Vehicle> responseVehicles)
        {
            var dbVehicles = DbContext.Vehicles.Where(x => x.ProviderName == _providerName);

            //Check if Vehicles from request contains vehicle in db otherwise delete them
            foreach (var vehicle in dbVehicles)
            {
                if (!responseVehicles.Contains(vehicle))
                {
                    DbContext.Vehicles.Remove(vehicle);
                }
            }

            DbContext.SaveChanges();
        }
    }
}