﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using AutoMapper;
using iMHD.Contract.Model;
using KordisReference;

namespace iMHD.DataProvider
{
    public class AutoMapperProfileConfiguration : Profile
    {
        public AutoMapperProfileConfiguration()
        {
            CreateMap<Vehicle, Vehicle>();
            CreateMap<Station, Station>();
            CreateMap<string[], Vehicle>()
                .ForMember(p => p.VehicleId, opts => opts.MapFrom(s => int.Parse(s[0])))
                .ForMember(p => p.LastStation, opts => opts.MapFrom(s => ParseStationId(s[1])))
                .ForMember(p => p.Delay, opts => opts.MapFrom(s => int.Parse(s[2])))
                .ForMember(p => p.LineName, opts => opts.MapFrom(s => s[3]))
                .ForMember(p => p.ServiceNumber, opts => opts.MapFrom(s => s[4]))
                .ForMember(p => p.Course, opts => opts.MapFrom(s => s[5]))
                .ForMember(p => p.StartingStation, opts => opts.MapFrom(s => ParseStationId(s[7])))
                .ForMember(p => p.FinalStation, opts => opts.MapFrom(s => ParseStationId(s[8])))
                .ForMember(p => p.Lon, opts => opts.MapFrom(s => float.Parse(s[9], CultureInfo.InvariantCulture)))
                .ForMember(p => p.Lat, opts => opts.MapFrom(s => float.Parse(s[10], CultureInfo.InvariantCulture)))
                .ForMember(p => p.LastUpdate, opts => opts.MapFrom(s => DateTime.ParseExact(s[11], "HH:mm:ss dd.MM.yyyy",
                        CultureInfo.InvariantCulture)))
                .ForMember(p => p.Azimuth, opts => opts.MapFrom(s => double.Parse(s[12])));

            CreateMap<TrafficStateResp.Entry, Vehicle>()
                .ForMember(p => p.VehicleId, opts => opts.MapFrom(s => s.CarNum))
                .ForMember(p => p.LastStation, opts => opts.MapFrom(s => s.LastStopID))
                .ForMember(p => p.Delay, opts => opts.MapFrom(s => s.DelayInMins))
                .ForMember(p => p.LineName, opts => opts.MapFrom(s => s.LineID))
                .ForMember(p => p.FinalStation, opts => opts.MapFrom(s => s.FinalStopID))
                .ForMember(p => p.Lat, opts => opts.MapFrom(s => s.Latitude))
                .ForMember(p => p.Lon, opts => opts.MapFrom(s => s.Longitude))
                .ForMember(p => p.LastStation, opts => opts.MapFrom(s => s.LastStopID))
                .ForMember(p => p.LastStation, opts => opts.MapFrom(s => s.LastStopID));

            CreateMap<string[], Station>()
                .ForMember(p => p.StationId, opts => opts.MapFrom(s => CombineNumers(s[0], s[1])))
                .ForMember(p => p.Name, opts => opts.MapFrom(s => s[2]))
                .ForMember(p => p.Lat, opts => opts.MapFrom(s => double.Parse(s[3], CultureInfo.InvariantCulture)))
                .ForMember(p => p.Lon, opts => opts.MapFrom(s => double.Parse(s[4], CultureInfo.InvariantCulture)))
                .ForMember(p => p.FirstDoorEntryOnly, opts => opts.MapFrom(s => int.Parse(s[5])))
                .ForMember(p => p.HandicappedStop, opts => opts.MapFrom(s => int.Parse(s[6])))
                .ForMember(p => p.PartiallyRequestStop, opts => opts.MapFrom(s => int.Parse(s[7])))
                .ForMember(p => p.RequestStop, opts => opts.MapFrom(s => int.Parse(s[8])))
                .ForMember(p => p.Zone, opts => opts.MapFrom(s => s[9]));

            CreateMap<MatchCollection, Station>()
                .ForMember(p => p.StationId, opts => opts.MapFrom(s => ParseStationId(s[0].Groups[1].Value)))
                .ForMember(p => p.Zone, opts => opts.MapFrom(s => s[0].Groups[2].Value))
                .ForMember(p => p.Name, opts => opts.MapFrom(s => s[0].Groups[3].Value));
        }

        private static int ParseStationId(string station)
        {
            return int.Parse(station);
        }

        private int CombineNumers(string num1, string num2)
        {
            var value = string.Format("{0}{1}",num1,num2);
            return int.Parse(value);
        }
    }
}